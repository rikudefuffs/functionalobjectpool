﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObjectPool
{
    public class ObjectPool<T> where T : class
    {
        public delegate T ObjectCreationCallback(params object[] parameters);

        List<T> pooledObjects;
        ObjectCreationCallback objectCreationCallback;
        Func<T, T> resetCallback;

        public ObjectPool(ObjectCreationCallback objectCreationCallback, Func<T, T> resetCallback)
        {
            pooledObjects = new List<T>();
            this.objectCreationCallback = objectCreationCallback;
            this.resetCallback = resetCallback;
        }

        public void Return(T objectToAdd)
        {
            if (pooledObjects.Contains(objectToAdd)) { return; }
            if (resetCallback == null) 
            {
                return; 
            }

            objectToAdd = resetCallback(objectToAdd);
            pooledObjects.Add(objectToAdd);
        }

        public T Get(params object[] newObjectDefaultValues)
        {
            if (pooledObjects.Count < 1)
            {
                //create something
                T newObject = objectCreationCallback(newObjectDefaultValues);
                return newObject;
            }
            T pooledObject = pooledObjects[0];
            pooledObjects.Remove(pooledObject);
            return pooledObject;
        }
    }
}
