﻿using System;

namespace ObjectPool
{
    class Program
    {
        static void Main(string[] args)
        {
            ObjectPool<PoolableObject> pool = new ObjectPool<PoolableObject>(PoolableObject.CreateNew, PoolableObject.Reset);
            for (int i = 0; i < 100000; i++)
            {
                PoolableObject generatedObject = pool.Get();
            }
        }
    }
}
