﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObjectPool
{
    class PoolableObject
    {
        public PoolableObject(params object[] initializationParameters)
        {
            //do something with the parameters
        }

        public static PoolableObject CreateNew(params object[] initializationParameters)
        {
            return new PoolableObject(initializationParameters);
        }

        public static PoolableObject Reset(PoolableObject objectToReset)
        {
            //do stuff for resetting
            return objectToReset;
        }
    }
}
